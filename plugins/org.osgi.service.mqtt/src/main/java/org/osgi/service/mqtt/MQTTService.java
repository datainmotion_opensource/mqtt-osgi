/*
 * Copyright (c) OSGi Alliance (2005, 2017). All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.osgi.service.mqtt;

import java.nio.ByteBuffer;

import org.osgi.util.pushstream.PushStream;

/**
 * MQTT service interface to handle MQTT related configurations
 * @author ${id}
 */
public interface MQTTService {
	
	/** Connection URL for the broker */
	public static final String PROP_BROKER = "mqtt.broker";
	/** MQTT topics that will be forwarded to EventAdmin */
	public static final String PROP_MQTT_SUBSCRIBE_TOPICS = "mqtt.subscribe.topics";
	public static final String PROP_MQTT_PUBLISH_TOPICS = "mqtt.publish.topics";
	/** Set to true, if it is allowed to pulish on subscribed topics */
	public static final String PROP_MQTT_PUBLISH_ON_SUBSCRIBE = "mqtt.publishOnSubcribe";
	
	/**
	 * Subscribe the {@link PushStream} to the given topic
	 * @param topic the MQTT topic to subscribe
	 * @return a {@link PushStream} instance for the given topic
	 */
	public PushStream<MQTTMessage> subscribe(String topic);
	
	/**
	 * Subscribe the {@link PushStream} to the given topic with a certain quality of service
	 * @param topic the MQTT topic to subscribe
	 * @param qos the {@link QoS} parameter
	 * @return a {@link PushStream} instance for the given topic
	 */
	public PushStream<MQTTMessage> subscribe(String topic, QoS qos);
	
	public void publish(String topic, ByteBuffer content) throws Exception;
	public void publish(String topic, ByteBuffer content, QoS qos) throws Exception;
	public void publishRetained(String topic, ByteBuffer content) throws Exception;

}
