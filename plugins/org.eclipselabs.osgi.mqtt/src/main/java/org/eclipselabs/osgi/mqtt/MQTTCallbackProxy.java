/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.osgi.mqtt;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Proxy object that delegates all calls to the subsequent registered callbacks
 * @author Mark Hoffmann
 * @since 22.04.2017
 */
public class MQTTCallbackProxy implements MqttCallback {

	private static Logger LOG = Logger.getLogger("org.eclipselabs.osgi.mqtt.mqttCallbackProxy");
	private final MqttClient client;
	private final List<String> topics;
	private final Map<String, List<MqttCallback>> proxyCallbacksMap = new ConcurrentHashMap<>();

	public MQTTCallbackProxy(MqttClient client, List<String> mqttTopics) {
		this.client = client;
		this.topics = mqttTopics == null ? Collections.emptyList() : mqttTopics;
	}

	/**
	 * Adds a given callback listener for the given topic to the proxy
	 * @param topic the topic to register the callback for
	 * @param callback the callback to register
	 * @return <code>true</code>, if the callback handler was registered
	 */
	public boolean addCallbackListener(String topic, MqttCallback callback) {
		if (callback == null || 
				topic == null || 
				!topics.contains(topic)) {
			return false;
		}
		List<MqttCallback> callbacks = proxyCallbacksMap.get(topic);
		if (callbacks == null) {
			callbacks = Collections.synchronizedList(new LinkedList<>());
			proxyCallbacksMap.put(topic, callbacks);
		}
		boolean added = callbacks.add(callback);
		// if the first elements was added to the callback, we lazy subscribe the topic
		if (added && callbacks.size() == 1) {
			return added && subscribe(topic);
		} else {
			return added;
		}
	}

	/**
	 * Removes the given callback from the listener list
	 * @param topic the topic the callback is responsible for
	 * @param callback the callback to be removed
	 * @return <code>true</code>, if the callback was removed from the proxy
	 */
	public boolean removeCallbackListener(String topic, MqttCallback callback) {
		if (callback == null || 
				topic == null || 
				!topics.contains(topic)) {
			return false;
		}
		List<MqttCallback> callbacks = proxyCallbacksMap.get(topic);
		if (callbacks != null && !callbacks.isEmpty()) {
			boolean removed = callbacks.remove(callback);
			// unsubscribe lazy all unused topics
			if (callbacks.isEmpty()) {
				return unsubscribe(topic) && removed;
			} else {
				return removed;
			}
		}
		return false;
	}
	
	/**
	 * Cleanup all resources
	 */
	public void dispose() {
		try {
			client.setCallback(null);
			if (!topics.isEmpty()) {
				client.unsubscribe(topics.toArray(new String[topics.size()]));
			}
		} catch (MqttException e) {
			LOG.log(Level.SEVERE, "[" + client.getClientId() + "] Error unsubscribing topics from client: " + topics, e);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "[" + client.getClientId() + "] Error disposing MQTTCallbackProxy from client", e);
		}
		
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.Throwable)
	 */
	@Override
	public void connectionLost(Throwable cause) {
		// fire connection lost to all registered callbacks
		proxyCallbacksMap.forEach((k, cl) -> cl.stream().parallel().forEach((c)->c.connectionLost(cause)));
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho.client.mqttv3.IMqttDeliveryToken)
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		String[] deliveredTopicStrings = token.getTopics();
		List<String> deliveredTopics = deliveredTopicStrings == null ? Collections.emptyList() : Arrays.asList(deliveredTopicStrings);
		// forward delivery complete to all callbacks that are responsible for the tokens topics
		proxyCallbacksMap.forEach((topic, cl) -> {
			if (deliveredTopics.contains(topic)) {
				cl.stream().parallel().forEach((c)->c.deliveryComplete(token));
			}
		});
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String, org.eclipse.paho.client.mqttv3.MqttMessage)
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		// forward delivery complete to all callbacks that are responsible for the tokens topics
		proxyCallbacksMap.forEach((t, cl) -> {
			if (topic.equals(t)) {
				cl.stream().parallel().forEach((c)->{
					try {
						c.messageArrived(topic, message);
					} catch (Exception e) {
						LOG.log(Level.SEVERE, "[" + topic + "] Message arrived callback caused an exception " + c.getClass(), e);
					}
				});
			}
		});
	}
	
	/**
	 * Subscribes a topic to the mqtt client and return <code>true</code> on success, otherwise <code>false</code>
	 * @param topic the topic to subscribe
	 * @return <code>true</code>, if the topic was subscribed successfully
	 */
	private boolean subscribe(String topic) {
		if (topic != null && client != null) {
			try {
				client.subscribe(topic);
				return true;
			} catch (MqttException e) {
				LOG.log(Level.SEVERE, "[" + topic + "] Error subscribing topic to the client: " + client.getClientId(), e);
				return false;
			}
		}
		return false;
	}
	
	/**
	 * Subscribes a topic to the mqtt client and return <code>true</code> on success, otherwise <code>false</code>
	 * @param topic the topic to subscribe
	 * @return <code>true</code>, if the topic was subscribed successfully
	 */
	private boolean unsubscribe(String topic) {
		if (topic != null && client != null) {
			try {
				client.unsubscribe(topic);
				return true;
			} catch (MqttException e) {
				LOG.log(Level.SEVERE, "[" + topic + "] Error un-subscribing topic to the client: " + client.getClientId(), e);
				return false;
			}
		}
		return false;
	}

}
