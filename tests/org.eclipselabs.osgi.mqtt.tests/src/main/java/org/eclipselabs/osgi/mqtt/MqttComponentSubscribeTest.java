package org.eclipselabs.osgi.mqtt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.mqtt.MQTTMessage;
import org.osgi.service.mqtt.MQTTService;
import org.osgi.util.pushstream.PushStream;
import org.osgi.util.tracker.ServiceTracker;

public class MqttComponentSubscribeTest {

	private String brokerUrl = "tcp://iot.eclipse.org:1883";
	private MqttClient checkClient;
	private Configuration clientConfig = null;
	
	@Before
	public void setup() throws MqttException {
		checkClient = new MqttClient(brokerUrl, "test");
		checkClient.connect();
	}
	
	@After
	public void teardown() throws MqttException, IOException {
		checkClient.disconnect();
		checkClient.close();
		if (clientConfig != null) {
			clientConfig.delete();
			clientConfig = null;
		}
	}

	
	/**
	 * Tests publishing a message
	 * @throws Exception
	 */
	@Test
	public void testSubscribeMessage_NoMessage() throws Exception {
		BundleContext context = createBundleContext();
		final CountDownLatch createLatch = new CountDownLatch(1);
		clientConfig = getConfiguration(context, "MQTTService", createLatch);
		
		String publishTopic = "test.junit";
		String subscribeTopic = publishTopic;
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put(MQTTService.PROP_MQTT_SUBSCRIBE_TOPICS, subscribeTopic);
		p.put(MQTTService.PROP_BROKER, brokerUrl);
		
		// track the service
		TestServiceCustomizer<MQTTService, MQTTService> customizer = new TestServiceCustomizer<MQTTService, MQTTService>(context, createLatch);
		ServiceTracker<MQTTService, MQTTService> tracker = new ServiceTracker<MQTTService, MQTTService>(context, MQTTService.class, customizer);
		tracker.open(true);
		
		// starting adapter with the given properties
		clientConfig.update(p);
		
		createLatch.await(5, TimeUnit.SECONDS);
		
		// check for service
		assertEquals(1, customizer.getAddCount());
		MQTTService mqttService = tracker.waitForService(15000l);
		assertNotNull(mqttService);
		
		//send message and wait for the result
		PushStream<MQTTMessage> subscribeStream = mqttService.subscribe(publishTopic);
		assertNotNull(subscribeStream);
		subscribeStream.close();
		
	}
	
	/**
	 * Tests publishing a message
	 * @throws Exception
	 */
	@Test
	public void testSubscribeOftenMessage_NoMessage() throws Exception {
		BundleContext context = createBundleContext();
		final CountDownLatch createLatch = new CountDownLatch(1);
		clientConfig = getConfiguration(context, "MQTTService", createLatch);
		
		String publishTopic = "test.junit";
		String subscribeTopic = publishTopic;
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put(MQTTService.PROP_MQTT_SUBSCRIBE_TOPICS, subscribeTopic);
		p.put(MQTTService.PROP_BROKER, brokerUrl);
		
		// track the service
		TestServiceCustomizer<MQTTService, MQTTService> customizer = new TestServiceCustomizer<MQTTService, MQTTService>(context, createLatch);
		ServiceTracker<MQTTService, MQTTService> tracker = new ServiceTracker<MQTTService, MQTTService>(context, MQTTService.class, customizer);
		tracker.open(true);
		
		// starting adapter with the given properties
		clientConfig.update(p);
		
		createLatch.await(5, TimeUnit.SECONDS);
		
		// check for service
		assertEquals(1, customizer.getAddCount());
		MQTTService mqttService = tracker.waitForService(15000l);
		assertNotNull(mqttService);
		
		//send message and wait for the result
		PushStream<MQTTMessage> subscribeStream01 = mqttService.subscribe(publishTopic);
		assertNotNull(subscribeStream01);
		subscribeStream01.close();
		PushStream<MQTTMessage> subscribeStream02 = mqttService.subscribe(publishTopic);
		assertNotNull(subscribeStream02);
		subscribeStream02.close();
		assertFalse(subscribeStream01.equals(subscribeStream02));
		
	}
	
	/**
	 * Tests publishing a message
	 * @throws Exception
	 */
	@Test
	public void testSubscribeMessage_Message() throws Exception {
		BundleContext context = createBundleContext();
		final CountDownLatch createLatch = new CountDownLatch(1);
		clientConfig = getConfiguration(context, "MQTTService", createLatch);
		
		String publishTopic = "test.junit";
		String subscribeTopic = publishTopic;
		String publishContent = "this is a test";
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put(MQTTService.PROP_MQTT_SUBSCRIBE_TOPICS, subscribeTopic);
		p.put(MQTTService.PROP_BROKER, brokerUrl);
		
		// count down latch to wait for the message
		CountDownLatch resultLatch = new CountDownLatch(1);
		// holder for the result
		AtomicReference<String> result = new AtomicReference<>();
		
		// track the service
		TestServiceCustomizer<MQTTService, MQTTService> customizer = new TestServiceCustomizer<MQTTService, MQTTService>(context, createLatch);
		ServiceTracker<MQTTService, MQTTService> tracker = new ServiceTracker<MQTTService, MQTTService>(context, MQTTService.class, customizer);
		tracker.open(true);
		
		// starting adapter with the given properties
		clientConfig.update(p);
		
		createLatch.await(5, TimeUnit.SECONDS);
		
		// check for service
		assertEquals(1, customizer.getAddCount());
		MQTTService mqttService = tracker.waitForService(15000l);
		assertNotNull(mqttService);
		
		//send message and wait for the result
		PushStream<MQTTMessage> subscribeStream = mqttService.subscribe(publishTopic);
		
		subscribeStream.forEach((msg)->{
			byte[] c = msg.payload().array();
			result.set(new String(c));
		});
		
		publish(publishTopic, publishContent);
		// wait and compare the received message
		resultLatch.await(5, TimeUnit.SECONDS);
		assertEquals(publishContent, result.get());
		
	}
	
	/**
	 * Tests publishing a message
	 * @throws Exception
	 */
	@Test
	public void testSubscribeOftenMessage_Message() throws Exception {
		BundleContext context = createBundleContext();
		final CountDownLatch createLatch = new CountDownLatch(1);
		clientConfig = getConfiguration(context, "MQTTService", createLatch);
		
		String publishTopic = "test.junit";
		String subscribeTopic = publishTopic;
		String publishContent = "this is a test";
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put(MQTTService.PROP_MQTT_SUBSCRIBE_TOPICS, subscribeTopic);
		p.put(MQTTService.PROP_BROKER, brokerUrl);
		
		// count down latch to wait for the message
		CountDownLatch resultLatch = new CountDownLatch(1);
		// holder for the result
		AtomicReference<String> result01 = new AtomicReference<>();
		AtomicReference<String> result02 = new AtomicReference<>();
		
		// track the service
		TestServiceCustomizer<MQTTService, MQTTService> customizer = new TestServiceCustomizer<MQTTService, MQTTService>(context, createLatch);
		ServiceTracker<MQTTService, MQTTService> tracker = new ServiceTracker<MQTTService, MQTTService>(context, MQTTService.class, customizer);
		tracker.open(true);
		
		// starting adapter with the given properties
		clientConfig.update(p);
		
		createLatch.await(5, TimeUnit.SECONDS);
		
		// check for service
		assertEquals(1, customizer.getAddCount());
		MQTTService mqttService = tracker.waitForService(15000l);
		assertNotNull(mqttService);
		
		//send message and wait for the result
		PushStream<MQTTMessage> subscribeStream01 = mqttService.subscribe(publishTopic);
		PushStream<MQTTMessage> subscribeStream02 = mqttService.subscribe(publishTopic);
		
		subscribeStream01.forEach((msg)->{
			byte[] c = msg.payload().array();
			result01.set(new String(c));
		});
		subscribeStream02.forEach((msg)->{
			byte[] c = msg.payload().array();
			result02.set(new String(c));
		});
		
		publish(publishTopic, publishContent);
		// wait and compare the received message
		resultLatch.await(5, TimeUnit.SECONDS);
		assertEquals(publishContent, result01.get());
		assertEquals(publishContent, result02.get());
		assertEquals(result01.get(), result02.get());
		
	}
	/**
	 * Tests publishing a message with an invalid topic
	 * @throws Exception
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testSubscribeMessage_WrongTopic() throws Exception {
		BundleContext context = createBundleContext();
		final CountDownLatch createLatch = new CountDownLatch(1);
		clientConfig = getConfiguration(context, "MQTTService", createLatch);
		
		String publishTopic = "test.junit";
		String subscribeTopic = publishTopic;
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		// add service properties
		p = new Hashtable<>();
		p.put(MQTTService.PROP_MQTT_SUBSCRIBE_TOPICS, subscribeTopic);
		p.put(MQTTService.PROP_BROKER, brokerUrl);
		
		// track the service
		TestServiceCustomizer<MQTTService, MQTTService> customizer = new TestServiceCustomizer<MQTTService, MQTTService>(context, createLatch);
		ServiceTracker<MQTTService, MQTTService> tracker = new ServiceTracker<MQTTService, MQTTService>(context, MQTTService.class, customizer);
		tracker.open(true);
		
		// starting adapter with the given properties
		clientConfig.update(p);
		
		createLatch.await(5, TimeUnit.SECONDS);
		
		// check for service
		assertEquals(1, customizer.getAddCount());
		MQTTService mqttService = tracker.waitForService(15000l);
		assertNotNull(mqttService);
		
		//send message and wait for the result
		mqttService.subscribe("my.topic");
	}
	
	/**
	 * Publishes some content to a given topic
	 * @param topic
	 * @param messageString
	 * @throws MqttPersistenceException
	 * @throws MqttException
	 */
	private void publish(String topic, String messageString) throws MqttPersistenceException, MqttException {
		assertNotNull(topic);
		assertNotNull(messageString);
		MqttMessage message = new MqttMessage();
		message.setPayload(messageString.getBytes());
		checkClient.publish(topic, message);
	}
	
	/**
	 * Creates a configuration with the configuration admin
	 * @param context the bundle context
	 * @param configId the configuration id
	 * @param createLatch the create latch for waiting
	 * @return the configuration
	 * @throws Exception
	 */
	private Configuration getConfiguration(BundleContext context, String configId, CountDownLatch createLatch) throws Exception {
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create MQTT client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(configId);
		assertNotNull(clientConfig);
		
		return clientConfig;
	}
	
	/**
	 * Creates a bundle context
	 * @return the bundle context
	 * @throws BundleException
	 */
	private BundleContext createBundleContext() throws BundleException {
		Bundle bundle = FrameworkUtil.getBundle(MQTTAdapter.class);
		assertEquals("org.eclipselabs.osgi.mqtt", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		return context;
	}
	
}
